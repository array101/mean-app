var mongoose = require('mongoose');
var User = mongoose.model('User');

var bcrypt = require('bcrypt');
const saltRounds = 10;

var jwt = require('jsonwebtoken');
const jwtSecret = 'jobbunny'

// POST /api/users/register
module.exports.register = function(req, res) {
  console.log('registering user');

  var username = req.body.username;
  var name = req.body.name;
  var password = req.body.password;

  bcrypt.hash(password, saltRounds, function(err, hash) {
    // Store hash in your password DB.
    User
      .create({
        username: username,
        name: name,
        password: hash
      }, function(err, user){
        if (err) {
          console.log("Error creating user", err)
          res.status(400).json(err);
        } else {
          console.log("User created ", user);
          res.status(201).json(user);
        }
      });
  });

};

// POST /api/users/login
module.exports.login = function(req, res) {
  console.log('logging in user');

  var username = req.body.username;
  var password = req.body.password;

  User
    .findOne({ username: username })
    .exec(function(err, user) {
      if (err) {
        console.log(err)
        res.status(400).json(err);
      } else {
        if ( bcrypt.compareSync(password, user.password) ) {
          console.log('User found', user);
          // sign a token: jwt.sign(payload, secret_string, expiry_date)
          var token = jwt.sign({ username: user.username }, jwtSecret, { expiresIn: 3600 });
          res.status(200).json({ success: true, token: token });
        } else {
          res.status(401).json('Unauthorized');
        }
      }
    });
};

// authorization middleware
module.exports.authorize = function(req, res, next) {
  var headerExists = req.headers.authorization;
  if (headerExists) {
    // Authorization header: Authorization Bearer xxx (xxx -> token)
    var token = req.headers.authorization.split(' ')[1];
    // verify signed token
    jwt.verify(token, jwtSecret, function(err, decoded){
      if (err) {
        console.log(err);
        res.status(401).json('Unauthorized');
      } else {
        // pull username from signed payload
        req.user = decoded.username;
        next();
      }
    });
  } else {
    res.status(403).json('No token provided');
  }
};
