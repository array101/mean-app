var mongoose = require('mongoose');
var Hotel = mongoose.model('Hotel');

var runGeoQuery = function(req, res) {
  var long = parseFloat(req.query.long);
  var lat = parseFloat(req.query.lat);

  // a geoJSON point
  var point = {
    type : "Point",
    coordinates : [long, lat]
  }

  var geoOptions = {
    spherical : true,
    maxDistance : 2000,
    num : 5
  }

  Hotel
    .geoNear(point, geoOptions, function(err, results, stats){
      console.log('Geo results ', results);
      console.log('Geo stats ', stats);
      res
        .status(200)
        .json(results)
    });
}

module.exports.hotelsGetAll = function(req, res){
  var offset = 0;
  var count = 5;
  var maxCount = 10;

  if (req.query && req.query.lat && req.query.long) {
    runGeoQuery(req, res);
    return;
  }

  if (req.query && req.query.offset) {
    offset = parseInt(req.query.offset, 10);
  }
  if (req.query && req.query.count) {
    count = parseInt(req.query.count, 10);
  }

  if (isNaN(offset) || isNaN(count)) {
    res
      .status(400)
      .json({
        "message" : "If supplied in querystring, count and offset shoule be numbers"
      });
    return;
  }

  if (count > maxCount) {
    res
      .status(400)
      .json({
        "message" : "Count limit of " + maxCount + " exceeded."
      });
    return;
  }

  Hotel
    .find()
    .skip(offset)
    .limit(count)
    .exec(function(err, hotels){
      if (err) {
        res
          .status(500)
          .json(err);
      } else {
        console.log('Found hotels', hotels.length)
        res
          .status(200)
          .json(hotels);
      }
    });
};

module.exports.hotelsGetOne = function(req, res){
  var hotelId = req.params.hotelId;
  console.log('GET hotelId', hotelId);

  Hotel
    .findById(hotelId)
    .exec(function(err, doc){
      var response = {
        status : 200,
        message : doc
      }
      if (err) {
        console.log('Error finding hotel ', err);
        response.status = 500;
        response.message = err;
      } else if (!doc) {
        response.status = 404;
        response.message = "HotelId not found";
      } else {
        console.log('hotel found (ID)-', doc._id)
      }

      res
        .status(response.status)
        .json(response.message);
    });
};

var _splitArray = function(input) {
  var output;
  if (input && input.length > 0) {
    output = input.split(";")
  } else {
    output = []
  }
  return output;
}

module.exports.hotelsAddOne = function(req, res){
  Hotel
    .create({
      name : req.body.name,
      description : req.body.description,
      photos : _splitArray(req.body.photos),
      stars : parseInt(req.body.stars, 10),
      services : _splitArray(req.body.services),
      currency : req.body.currency,
      location : {
        address : req.body.address,
        coordinates: [
            parseFloat(req.body.long),
            parseFloat(req.body.lat)
          ]
      }
    }, function(err, hotel){
      if (err) {
        console.log("Error creating hotel")
        res
          .status(400)
          .json(err);
      } else {
        console.log("Hotel created ", hotel);
        res
          .status(201)
          .json(hotel);
      }
    });
};


module.exports.hotelsUpdateOne = function(req, res) {
  var hotelId = req.params.hotelId;
  console.log('GET hotelId', hotelId);

  Hotel
    .findById(hotelId)
    .select("-reviews -rooms")
    .exec(function(err, doc){
      var response = {
        status : 200,
        message : doc
      };

      if (err) {
        console.log('Error finding hotel ', err);
        response.status = 500;
        response.message = err;
      } else if (!doc) {
        response.status = 404;
        response.message = "HotelId not found";
      }

      if (response.status !== 200) {
        res
          .status(response.status)
          .json(response.message);
      } else {
        doc.name = req.body.name;
        doc.description = req.body.description;
        doc.photos = _splitArray(req.body.photos);
        doc.stars = parseInt(req.body.stars, 10);
        doc.services = _splitArray(req.body.services);
        doc.currency = req.body.currency;
        doc.location = {
          address : req.body.address,
          coordinates: [
              parseFloat(req.body.long),
              parseFloat(req.body.lat)
            ]
        };

        doc.save(function(err, updatedHotel){
          if (err) {
            res
              .status(500)
              .json(err)
          } else {
            res
              .status(204)
              .json();
          }
        });
      }
    });
};


module.exports.hotelsDeleteOne = function(req, res) {
  var hotelId = req.params.hotelId;

  Hotel
    .findByIdAndRemove(hotelId)
    .exec(function(err, hotel){
      if (err) {
        res
          .status(404)
          .json(err);
      } else {
        console.log('Hotel delted, id: ', hotelId);
        res
          .status(204)
          .json(err);
      }
    });
};







