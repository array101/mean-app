var mongoose = require('mongoose');
var dburl;

if (process.env.NODE_ENV == 'development') {
  dburl = 'mongodb://127.0.0.1:27017/meanhotel';
} else {
  dburl = process.env.MONGODB_URI;
}


mongoose.connect(dburl);

mongoose.connection.on('connected', function(){
  console.log('Mongoose is connected to ' + dburl);
});
mongoose.connection.on('disconnected', function(){
  console.log('Mongoose disconnected');
});
mongoose.connection.on('error', function(err){
  console.log('Mongoose connection error ' + err);
});


// node process event handler - close connection on app kill
// linux servers fire SIGINT
process.on('SIGINT', function() {
  mongoose.connection.close(function(){
    console.log('Mongoose connection disconnected through app termination');
    process.exit(0);
  });
});
// heroku fires SIGTERM
process.on('SIGTERM', function() {
  mongoose.connection.close(function(){
    console.log('Mongoose connection disconnected through app termination');
    process.exit(0);
  });
});
// nodemon fires SIGUSR2
process.once('SIGUSR2', function() {
  mongoose.connection.close(function(){
    console.log('Mongoose connection disconnected through app termination');
    process.kill(process.pid, 'SIGUSR2');
  });
});

// bring in models and schema
require('./hotels.model.js');
require('./users.model.js')


