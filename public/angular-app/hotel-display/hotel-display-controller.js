angular.module('meanhotel').controller('hotelDisplayController', hotelDisplayController);

function hotelDisplayController($route, $window, hotelDataFactory, $routeParams, AuthFactory, jwtHelper) {
  var vm = this;
  var id = $routeParams.id;

  hotelDataFactory.hotelDisplay(id).then(function(response){
    vm.hotel = response;
    vm.stars = _getStarRating(response.stars);
  });

  function _getStarRating(stars) {
    return new Array(stars);
  }

  vm.isLoggedIn = function() {
   if(AuthFactory.isLoggedIn) {
      return true;
    } else {
      return false;
    }
  }

  vm.addReview = function() {
    var token = jwtHelper.decodeToken($window.sessionStorage.token);
    var username = token.username;

    var postData = {
      name: username,
      rating: vm.rating,
      review: vm.review
    };
    if (vm.reviewForm.$valid) {
      hotelDataFactory.postReview(id, postData).then(function(response){
        // reload if post is success
        $route.reload();
      }).catch(function(error) {
        console.log(error);
      })
    } else {
      // for displaying errors
      vm.isSubmitted = true;
    }
  }
}
