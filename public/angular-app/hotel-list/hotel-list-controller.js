angular.module('meanhotel').controller('hotelsController', hotelsController);

function hotelsController(hotelDataFactory) {
  var vm = this;
  vm.title = 'All hotels';

  hotelDataFactory.hotelList().then(function(response){
    // console.log(response);
    vm.hotels = response;
  });
}